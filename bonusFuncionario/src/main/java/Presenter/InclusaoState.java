/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Model.Funcionario;
import Presenter.Command.SalvarCommand;
import View.ManterFuncionario.P0101;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import javax.swing.JOptionPane;

/**
 *
 * @author Davidson
 */
public class InclusaoState extends PresenterState {

    public InclusaoState(P0101Presenter presenter) {
        super(presenter);

        super.getPresenter().removerListener();

        presenter.getViewManterFuncionario().getBotaoSalvarBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getBotaoExcluirBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getBotaoEditarBuscarFuncionario().setEnabled(false);

        presenter.getViewManterFuncionario().getNomeBuscarFuncionario().setText("");
        presenter.getViewManterFuncionario().getNomeBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getIdadeBuscarFuncionario().setText("");
        presenter.getViewManterFuncionario().getIdadeBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getSalarioBuscarFuncionario().setText("");
        presenter.getViewManterFuncionario().getSalarioBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getFaltasBuscarFuncionario().setText("");
        presenter.getViewManterFuncionario().getFaltasBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getAdimissaoBuscarFuncionario().setText("");
        presenter.getViewManterFuncionario().getAdimissaoBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getCargoBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getBonusBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getFuncionarioDoMesBuscarFuncionario().setEnabled(true);

        inicialConfirmar(super.getPresenter().getViewManterFuncionario());
    }

    public void inicialConfirmar(P0101 telaP0101) {
        telaP0101.getBotaoSalvarBuscarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                salvar();

            }
        });
    }

    @Override
    public void salvar() {

        int cargo = presenter.getViewManterFuncionario().getCargoBuscarFuncionario().getSelectedIndex();
        int bonus = presenter.getViewManterFuncionario().getBonusBuscarFuncionario().getSelectedIndex();
        Boolean funcionarioMes = presenter.getViewManterFuncionario().getFuncionarioDoMesBuscarFuncionario().isSelected();
        String nome = presenter.getViewManterFuncionario().getNomeBuscarFuncionario().getText();
        int idade = Integer.parseInt(presenter.getViewManterFuncionario().getIdadeBuscarFuncionario().getText());
        float salario = Float.parseFloat(presenter.getViewManterFuncionario().getSalarioBuscarFuncionario().getText());
        int faltas = Integer.parseInt(presenter.getViewManterFuncionario().getFaltasBuscarFuncionario().getText());
        LocalDate admissao = LocalDate.of(Integer.parseInt(presenter.getViewManterFuncionario().getAdimissaoBuscarFuncionario().getText().substring(6, 10)),
                Integer.parseInt(presenter.getViewManterFuncionario().getAdimissaoBuscarFuncionario().getText().substring(3, 5)),
                Integer.parseInt(presenter.getViewManterFuncionario().getAdimissaoBuscarFuncionario().getText().substring(0, 2)));

        Funcionario funcionario = new Funcionario(cargo, nome, idade, bonus, salario, faltas, funcionarioMes, admissao);
        super.getPresenter().setFuncionario(funcionario);

        new SalvarCommand().execute(super.getPresenter());
        JOptionPane.showMessageDialog(presenter.getViewManterFuncionario(), "Inserido com sucesso");
        super.fecharTela(super.getPresenter().getViewManterFuncionario());
    }

    @Override
    public void atualizar() {
        //Cancelar registro
        // presenter.getView().dispose();
    }
}
