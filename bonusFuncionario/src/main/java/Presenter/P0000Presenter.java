/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.DadoCollection;
import Model.Funcionario;
import Model.Observer.IModelObserver;
import View.P0000;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 *
 * @author Davidson
 */
public class P0000Presenter implements IModelObserver{
    private P0000 tela;

    public P0000Presenter() {
        DadoCollection.getIntances().adicionarObserver(this);
    }
 
    public void abrirTelaInicial() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(P0000.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(P0000.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(P0000.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(P0000.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        tela = new P0000();
        tela.setVisible(true);

        //Manter Funcionario - P0101
        tela.getManterFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                P0101Presenter presenterP0101 = new P0101Presenter(null);
                presenterP0101.manterFuncionario(tela); //funcao do botão de buscar funcionario

            }
        });

        //Buscar Funcionario - P0201
        tela.getBuscarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                P0201Presenter presenterP0201 = new P0201Presenter();
                presenterP0201.buscarFuncionario(tela); //funcao do botão de calcular salario
            }
        });
        
        //Calcular Salario - P0301
        tela.getCalcularSalario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                P0301Presenter presenterP0301 = new P0301Presenter();
                presenterP0301.calcularSalario(tela); //função do botão de manter funcionario
            }
        });

    }

    @Override
    public void updateObserver(List<Funcionario> funcionarioObserver) {
        int tamanho = funcionarioObserver.size();
        System.out.println("chegou1");
        tela.getFuncionariosCadastrados().setText(String.valueOf(tamanho));      
    }
}