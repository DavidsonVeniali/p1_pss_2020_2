/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Model.Funcionario;
import View.ManterFuncionario.P0101;
import View.P0000;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Davidson
 */
public class P0101Presenter {

    private P0101 viewManterFuncionario;
    private PresenterState estado;
    private Funcionario funcionario;

    public P0101Presenter(Funcionario funcionario) {
        viewManterFuncionario = new P0101();
        if (funcionario != null) {
            this.funcionario = funcionario;
            this.estado = new VisualizacaoState(this);
        } else {
            this.estado = new InclusaoState(this);
        }
    }

    public void removerListener() {

        if (viewManterFuncionario.getBotaoSalvarBuscarFuncionario().getActionListeners() != null) {
            for (ActionListener actionListener : viewManterFuncionario.getBotaoSalvarBuscarFuncionario().getActionListeners()) {
                viewManterFuncionario.getBotaoSalvarBuscarFuncionario().removeActionListener(actionListener);
            }
        }
        if (viewManterFuncionario.getBotaoEditarBuscarFuncionario().getActionListeners() != null) {
            for (ActionListener actionListener : viewManterFuncionario.getBotaoEditarBuscarFuncionario().getActionListeners()) {
                viewManterFuncionario.getBotaoEditarBuscarFuncionario().removeActionListener(actionListener);
            }
        }
        if (viewManterFuncionario.getBotaoExcluirBuscarFuncionario().getActionListeners() != null) {
            for (ActionListener actionListener : viewManterFuncionario.getBotaoExcluirBuscarFuncionario().getActionListeners()) {
                viewManterFuncionario.getBotaoExcluirBuscarFuncionario().removeActionListener(actionListener);
            }
        }

    }

    //BOTÃO CRIAR NOVA REPUBLICA - P0101
    public void manterFuncionario(P0000 desktop/*, usuario*/) {
        desktop.getDesktop().add(viewManterFuncionario);
        viewManterFuncionario.setVisible(true);

        viewManterFuncionario.getBotaoFecharBuscarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE CONFIRMAR A INCLUSÃO*/
                viewManterFuncionario.setVisible(false);
                viewManterFuncionario.dispose();
            }
        });
    }

    public void salvar() {
        estado.salvar();
    }

    public void atualizar() {
        estado.atualizar();
    }

    public void editar() {
        estado.editar();
    }

    public void excluir() {
        estado.excluir();
    }

    public void recuperar() {
        estado.excluir();
    }

    public void setEstado(PresenterState estado) {
        this.estado = estado;
    }

    public P0101 getViewManterFuncionario() {
        return viewManterFuncionario;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }
    
    
}
