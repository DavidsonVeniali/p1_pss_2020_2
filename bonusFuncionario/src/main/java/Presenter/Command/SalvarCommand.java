/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Command;

import Collection.DadoCollection;
import Presenter.P0101Presenter;

/**
 *
 * @author Davidson
 */
public class SalvarCommand extends AbsctractCommand {
    
    @Override
    public void execute(P0101Presenter p0101){
        DadoCollection.getIntances().add(p0101.getFuncionario());
    }

}
