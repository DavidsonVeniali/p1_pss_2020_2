/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Command;

import Collection.DadoCollection;
import Presenter.P0101Presenter;

/**
 *
 * @author Davidson
 */
public class ExcluirCommand extends AbsctractCommand {
    @Override
    public void execute(P0101Presenter p0101){
        DadoCollection.getIntances().delete(p0101.getFuncionario());
    }  
}
