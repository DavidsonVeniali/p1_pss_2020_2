/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter.Command;

import Presenter.P0101Presenter;

/**
 *
 * @author Davidson
 */
public abstract class AbsctractCommand {
    
    public abstract void execute (P0101Presenter p0101);
    
}
