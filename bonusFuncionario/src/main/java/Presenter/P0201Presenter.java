/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.DadoCollection;
import Model.Funcionario;
import View.BuscarFuncionario.P0201;
import View.P0000;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Frame;
import java.util.List;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Davidson
 */
public class P0201Presenter {

    private P0201 viewBuscarFuncionario;

    public P0201Presenter() {
        viewBuscarFuncionario = new P0201();
    }

    //BOTÃO CRIAR NOVA REPUBLICA - P0101
    public void buscarFuncionario(P0000 desktop/*, usuario*/) {
        desktop.getDesktop().add(viewBuscarFuncionario);
        viewBuscarFuncionario.setVisible(true);

        viewBuscarFuncionario.getBotaoBuscarCalcularSalario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nomeBuscaFuncionario = viewBuscarFuncionario.getNomeCalcularSalario().getText();

                List<Funcionario> funcionarioNoBanco = DadoCollection.getIntances().read(nomeBuscaFuncionario);

                JTable tabela = viewBuscarFuncionario.getTabelaCalcularSalario();
                DefaultTableModel modeloTabela = (DefaultTableModel) tabela.getModel();
                modeloTabela.setRowCount(0);/*zerando a tabela*/
                for (Funcionario funcionario : funcionarioNoBanco) {
                    modeloTabela.addRow(new Object[]{funcionario.getId(), funcionario.getNome(), funcionario.getIdade(), funcionario.getCargo(), funcionario.getSalario()});
                }
            }
        });
        
        viewBuscarFuncionario.getBotaoVisualizarCalcularSalario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTable tabela = viewBuscarFuncionario.getTabelaCalcularSalario();
                DefaultTableModel modeloTabela = (DefaultTableModel) tabela.getModel();
                Vector vetorTabela = modeloTabela.getDataVector().elementAt(tabela.getSelectedRow()); 
                
                
                Funcionario funcionario = DadoCollection.getIntances().readId(Integer.parseInt(vetorTabela.get(0).toString()));
                System.out.println(funcionario.getId());
                P0101Presenter presenterP0101 = new P0101Presenter(funcionario);
                presenterP0101.manterFuncionario(desktop);
            }
        });

        viewBuscarFuncionario.getBotaoNovoCalcularSalario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE CONFIRMAR A INCLUSÃO*/
                P0101Presenter presenterP0101 = new P0101Presenter(null);
                presenterP0101.manterFuncionario(desktop); //funcao do botão de buscar funcionario
            }
        });

        viewBuscarFuncionario.getBotaoFecharCalcularSalario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE CONFIRMAR A INCLUSÃO*/
                viewBuscarFuncionario.setVisible(false);
                viewBuscarFuncionario.dispose();
            }
        });

        viewBuscarFuncionario.getBotaoVerBonusCalcularSalario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE CONFIRMAR A INCLUSÃO*/
                P0202Presenter presenterP0202 = new P0202Presenter(new Frame(), true);
                presenterP0202.viewBonusFuncionario(desktop); //funcao do botão de buscar funcionario
            }
        });
    }
}
