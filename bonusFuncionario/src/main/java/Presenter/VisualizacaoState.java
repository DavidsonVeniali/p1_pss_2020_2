/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Presenter.Command.ExcluirCommand;
import View.ManterFuncionario.P0101;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Davidson
 */
public class VisualizacaoState extends PresenterState {

    public VisualizacaoState(P0101Presenter presenter) {
        super(presenter);

        super.getPresenter().removerListener();

        presenter.getViewManterFuncionario().getBotaoSalvarBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getBotaoExcluirBuscarFuncionario().setEnabled(true);
        presenter.getViewManterFuncionario().getBotaoEditarBuscarFuncionario().setEnabled(true);

        presenter.getViewManterFuncionario().getNomeBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getIdadeBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getSalarioBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getFaltasBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getAdimissaoBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getCargoBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getBonusBuscarFuncionario().setEnabled(false);
        presenter.getViewManterFuncionario().getFuncionarioDoMesBuscarFuncionario().setEnabled(false);

        presenter.getViewManterFuncionario().getNomeBuscarFuncionario().setText(presenter.getFuncionario().getNome());
        presenter.getViewManterFuncionario().getIdadeBuscarFuncionario().setText(String.valueOf(presenter.getFuncionario().getIdade()));
        presenter.getViewManterFuncionario().getFaltasBuscarFuncionario().setText(String.valueOf(presenter.getFuncionario().getFalta()));
        presenter.getViewManterFuncionario().getSalarioBuscarFuncionario().setText(String.valueOf(presenter.getFuncionario().getSalario()));
        presenter.getViewManterFuncionario().getCargoBuscarFuncionario().setSelectedIndex(presenter.getFuncionario().getCargo());
        presenter.getViewManterFuncionario().getBonusBuscarFuncionario().setSelectedIndex(presenter.getFuncionario().getBonus());
        presenter.getViewManterFuncionario().getFuncionarioDoMesBuscarFuncionario().setSelected(presenter.getFuncionario().getFuncionarioMes());
        presenter.getViewManterFuncionario().getAdimissaoBuscarFuncionario().setText(String.valueOf(presenter.getFuncionario().getAdmissao().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));

        inicia(super.getPresenter().getViewManterFuncionario());

    }

    public void inicia(P0101 telaP0101) {
        telaP0101.getBotaoEditarBuscarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editar();

            }
        });

        telaP0101.getBotaoExcluirBuscarFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                excluir();

            }
        });
    }

    @Override
    public void excluir() {
        new ExcluirCommand().execute(super.getPresenter());
        super.fecharTela(super.getPresenter().getViewManterFuncionario());
    }

    @Override
    public void editar() {
        super.getPresenter().setEstado(new EdicaoState(super.getPresenter()));
    }
}
