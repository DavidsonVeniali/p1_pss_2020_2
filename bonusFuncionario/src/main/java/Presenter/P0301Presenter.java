/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import Collection.DadoCollection;
import Model.AplicacaoBonus;
import View.CalcularSalario.P0301;
import View.P0000;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Davidson
 */
public class P0301Presenter {

    private P0301 viewCalcularSalario;
    private LocalDate dataDaBusca;

    public P0301Presenter() {
        viewCalcularSalario = new P0301();
    }

    //BOTÃO CRIAR NOVA REPUBLICA - P0101
    public void calcularSalario(P0000 desktop/*, usuario*/) {
        desktop.getDesktop().add(viewCalcularSalario);
        viewCalcularSalario.setVisible(true);
        /*viewManterFuncionario.getConfirmar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE CONFIRMAR A INCLUSÃO
                confirmar(/*usuario);
            }
        });*/

        viewCalcularSalario.getBotaoBuscarManterFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                LocalDate dataDaBusca = LocalDate.of(Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(6, 10)),
                        Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(3, 5)),
                        Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(0, 2)));
                //imprime como yyyy/MM/dd

                JTable tabela = viewCalcularSalario.getTabelaManterFuncionario();
                DefaultTableModel modeloTabela = (DefaultTableModel) tabela.getModel();

                List<AplicacaoBonus> BonusNoBanco = DadoCollection.getIntances().readBonus(dataDaBusca);

                for (AplicacaoBonus bonus : BonusNoBanco) {
                    modeloTabela.addRow(new Object[]{bonus.getNome(), bonus.getAdmissao(), bonus.getSalario(), bonus.getBonus(), bonus.getRecebeBonus()});
                }

            }
        });

        viewCalcularSalario.getBotaoCalculoManterFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //Data recebida da label superior, data de adimissão
                LocalDate dataDaBusca = LocalDate.of(Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(6, 10)),
                        Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(3, 5)),
                        Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(0, 2)));
                //imprime como yyyy/MM/dd

                //Data recebida da labem inferior, data para calculo do bonus
                LocalDate dataDoCalculo = LocalDate.of(Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(6, 10)),
                        Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(3, 5)),
                        Integer.parseInt(viewCalcularSalario.getDataBuscarManterFuncionario().getText().substring(0, 2)));
                
                //imprime como yyyy/MM/dd

                JTable tabela = viewCalcularSalario.getTabelaManterFuncionario();
                tabela.getSelectedRow();
                DefaultTableModel modeloTabela = (DefaultTableModel) tabela.getModel();
                AplicacaoBonus aplicacaoBonus = new AplicacaoBonus();
                List<AplicacaoBonus> BonusNoBanco = DadoCollection.getIntances().readBonus(dataDaBusca);
                
                for (AplicacaoBonus bonus : BonusNoBanco) {
                    DadoCollection.getIntances().updateBonus(bonus, bonus.getId(), dataDoCalculo);
                    modeloTabela.setRowCount(0);
                    aplicacaoBonus.BonusPorAssiduidade();
                    aplicacaoBonus.BonusPorCargo();
                    aplicacaoBonus.BonusPorFuncionarioDoMes();
                    aplicacaoBonus.BonusPorTempoDeServico();
                    modeloTabela.addRow(new Object[]{bonus.getNome(), bonus.getAdmissao(), bonus.getSalario(), bonus.getBonus(), bonus.getRecebeBonus()});  
                }
              
            }
         
        });

        viewCalcularSalario.getBotaoListarTodosManterFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JTable tabela = viewCalcularSalario.getTabelaManterFuncionario();
                DefaultTableModel modeloTabela = (DefaultTableModel) tabela.getModel();
                modeloTabela.setRowCount(0);

                List<AplicacaoBonus> BonusNoBanco = DadoCollection.getIntances().getListBonus();
                for (AplicacaoBonus bonus : BonusNoBanco) {
                    modeloTabela.addRow(new Object[]{bonus.getNome(), bonus.getAdmissao(), bonus.getSalario(), bonus.getBonus(), bonus.getRecebeBonus()});
                }
            }
        });

        viewCalcularSalario.getBotaoFecharManterFuncionario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //BOTÃO DE CONFIRMAR A INCLUSÃO*/
                viewCalcularSalario.setVisible(false);
                viewCalcularSalario.dispose();
            }
        });
    }

    public P0301 getViewCalcularSalario() {
        return viewCalcularSalario;
    }

    public void setViewCalcularSalario(P0301 viewCalcularSalario) {
        this.viewCalcularSalario = viewCalcularSalario;
    }

    public LocalDate getDataDaBusca() {
        return dataDaBusca;
    }

    public void setDataDaBusca(LocalDate dataDaBusca) {
        this.dataDaBusca = dataDaBusca;
    }
    
}
