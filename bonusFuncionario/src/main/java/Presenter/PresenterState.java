/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presenter;

import View.ManterFuncionario.P0101;

/**
 *
 * @author Davidson
 */
public abstract class PresenterState {

    P0101Presenter presenter;

    public PresenterState(P0101Presenter presenter) {
        this.presenter = presenter;
    }

    public void salvar() {
        throw new RuntimeException("Não é possível salvar a partir desse estado");
    }

    public void atualizar() {
        throw new RuntimeException("Não é possível cancelar a partir desse estado");
    }

    public void editar() {
        throw new RuntimeException("Não é possível editar a partir desse estado");
    }

    public void excluir() {
        throw new RuntimeException("Não é possível excluir a partir desse estado");
    }
    
    public void fecharTela(P0101 p0101){
        p0101.dispose();
    }

    public P0101Presenter getPresenter() {
        return presenter;
    }
    
    
}
