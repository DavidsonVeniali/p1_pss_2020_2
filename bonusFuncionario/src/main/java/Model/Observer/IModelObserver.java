/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Observer;

import Model.Funcionario;
import java.util.List;

/**
 *
 * @author Davidson
 */
public interface IModelObserver {
    public void updateObserver (List<Funcionario>  funcionarioObserver);
}
