/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Observer;

import Collection.DadoCollection;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Davidson
 */
public abstract class Subject {
    private List<IModelObserver> observers = new ArrayList();
    
    public void adicionarObserver(IModelObserver obs1){
        this.observers.add(obs1);
    }
    
    public void notificarObserver(){
        for (IModelObserver observer : observers) {
            observer.updateObserver(DadoCollection.getIntances().getListFuncionario());
        }
    }

}
