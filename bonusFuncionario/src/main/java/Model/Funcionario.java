/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;

/**
 *
 * @author Davidson
 */
public class Funcionario {
    
    private int id;
    private int cargo;
    private String nome;
    private int idade;
    private int bonus;
    private double salario;
    private int falta;
    private Boolean funcionarioMes;
    private LocalDate admissao;

    public Funcionario(int cargo, String nome, int idade, int bonus, double salario, int falta, Boolean funcionarioMes, LocalDate admissao) {
        this.cargo = cargo;
        this.nome = nome;
        this.idade = idade;
        this.bonus = bonus;
        this.salario = salario;
        this.falta = falta;
        this.funcionarioMes = funcionarioMes;
        this.admissao = admissao;
    }

    public int getCargo() {
        return cargo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getFalta() {
        return falta;
    }

    public void setFalta(int falta) {
        this.falta = falta;
    }

    public Boolean getFuncionarioMes() {
        return funcionarioMes;
    }

    public void setFuncionarioMes(Boolean funcionarioMes) {
        this.funcionarioMes = funcionarioMes;
    }

    public LocalDate getAdmissao() {
        return admissao;
    }

    public void setAdmissao(LocalDate admissao) {
        this.admissao = admissao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    

}
