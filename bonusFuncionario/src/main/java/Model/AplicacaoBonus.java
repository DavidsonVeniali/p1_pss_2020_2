/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;

/**
 *
 * @author Davidson
 */
public class AplicacaoBonus {

    private int id;
    private int cargo;
    private String nome;
    private int idade;
    private int bonus;
    private double salario;
    private int falta;
    private Boolean funcionarioMes;
    private LocalDate admissao;
    private double recebeBonus;
    private double porcentagemBonusAssinuidade = 0;
    private double porcentagemBonusTempodeServico = 0;
    private double porcentagemBonusFuncionarioDoMes = 0;
    private double porcentagemBonusPorCargo = 0;
    private LocalDate dataDeCalculo;

    public AplicacaoBonus() {
    }

    public double BonusPorAssiduidade() {

        if (falta == 0) {
            recebeBonus += salario * 0.1;
            porcentagemBonusAssinuidade = 0.1;
        } else if (falta >= 1 && falta <= 3) {
            recebeBonus += salario * 0.05;
            porcentagemBonusAssinuidade = 0.05;
        } else if (falta >= 4 && falta <= 5) {
            recebeBonus += salario * 0.01;
            porcentagemBonusAssinuidade = 0.01;
        } else {
            recebeBonus += salario * 0.01;
            porcentagemBonusAssinuidade = 0.01;
        }
        
        return recebeBonus;
    }

    public double BonusPorTempoDeServico() {

        if (admissao.getYear() == dataDeCalculo.getYear()) {
            recebeBonus += 0;
        } else if ((admissao.getYear() - dataDeCalculo.getYear()) >= 1 && (admissao.getYear() - dataDeCalculo.getYear()) <= 5) {
            recebeBonus += salario * 0.02;
            porcentagemBonusTempodeServico = 0.02;
        } else if ((admissao.getYear() - dataDeCalculo.getYear()) >= 6 && (admissao.getYear() - dataDeCalculo.getYear()) <= 10) {
            recebeBonus += salario * 0.03;
            porcentagemBonusTempodeServico = 0.03;
        } else if ((admissao.getYear() - dataDeCalculo.getYear()) >= 11 && (admissao.getYear() - dataDeCalculo.getYear()) <= 15) {
            recebeBonus += salario * 0.08;
            porcentagemBonusTempodeServico = 0.08;
        } else if ((admissao.getYear() - dataDeCalculo.getYear()) >= 16 && (admissao.getYear() - dataDeCalculo.getYear()) <= 20) {
            recebeBonus += salario * 0.1;
            porcentagemBonusTempodeServico = 0.1;
        } else {
            recebeBonus += salario * 0.15;
            porcentagemBonusTempodeServico = 0.15;
        }
        return recebeBonus;
    }

    public double BonusPorFuncionarioDoMes() {

        if (funcionarioMes.TRUE) {
            recebeBonus += salario * 0.3;
            porcentagemBonusFuncionarioDoMes = 0.3;
        }
        return recebeBonus;
    }

    public double BonusPorCargo() {

        if (cargo == 0) {
            recebeBonus += salario * 0.1;
            porcentagemBonusPorCargo = 0.1;
        } else if (cargo == 1) {
            recebeBonus += salario * 0.2;
            porcentagemBonusPorCargo = 0.2;
        } else {
            recebeBonus += salario * 0.3;
            porcentagemBonusPorCargo = 0.3;
        }
        return recebeBonus;
    }

    public AplicacaoBonus(int id, int cargo, String nome, int idade, int bonus, double salario, int falta, Boolean funcionarioMes, LocalDate admissao, double recebeBonus) {
        this.id = id;
        this.cargo = cargo;
        this.nome = nome;
        this.idade = idade;
        this.bonus = bonus;
        this.salario = salario;
        this.falta = falta;
        this.funcionarioMes = funcionarioMes;
        this.admissao = admissao;
        this.recebeBonus = recebeBonus;
    }

    public AplicacaoBonus(String nome, int bonus, double salario, LocalDate admissao) {
        this.nome = nome;
        this.bonus = bonus;
        this.salario = salario;
        this.admissao = admissao;
    }

    public AplicacaoBonus(String nome, int bonus, double salario, LocalDate admissao, double recebeBonus) {
        this.nome = nome;
        this.bonus = bonus;
        this.salario = salario;
        this.admissao = admissao;
        this.recebeBonus = recebeBonus;
    }

    public AplicacaoBonus(int cargo, String nome, int bonus, double salario, int falta, Boolean funcionarioMes, LocalDate admissao) {
        this.cargo = cargo;
        this.nome = nome;
        this.bonus = bonus;
        this.salario = salario;
        this.falta = falta;
        this.funcionarioMes = funcionarioMes;
        this.admissao = admissao;
    }
    
    public AplicacaoBonus(int cargo, String nome, int bonus, double salario, int falta, Boolean funcionarioMes, LocalDate admissao, LocalDate dataDoCalculo) {
        this.cargo = cargo;
        this.nome = nome;
        this.bonus = bonus;
        this.salario = salario;
        this.falta = falta;
        this.funcionarioMes = funcionarioMes;
        this.admissao = admissao;
        this.dataDeCalculo = dataDoCalculo;
    }
    
    public AplicacaoBonus(int id, int cargo, String nome, int bonus, double salario, int falta, Boolean funcionarioMes, LocalDate admissao) {
        this.id = id;
        this.cargo = cargo;
        this.nome = nome;
        this.bonus = bonus;
        this.salario = salario;
        this.falta = falta;
        this.funcionarioMes = funcionarioMes;
        this.admissao = admissao;
    }
    
    public AplicacaoBonus(int id, int cargo, String nome, int bonus, double salario, int falta, Boolean funcionarioMes, LocalDate admissao, LocalDate dataDoCalculo) {
        this.id = id;
        this.cargo = cargo;
        this.nome = nome;
        this.bonus = bonus;
        this.salario = salario;
        this.falta = falta;
        this.funcionarioMes = funcionarioMes;
        this.admissao = admissao;
        this.dataDeCalculo = dataDoCalculo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCargo() {
        return cargo;
    }

    public void setCargo(int cargo) {
        this.cargo = cargo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getFalta() {
        return falta;
    }

    public void setFalta(int falta) {
        this.falta = falta;
    }

    public Boolean getFuncionarioMes() {
        return funcionarioMes;
    }

    public void setFuncionarioMes(Boolean funcionarioMes) {
        this.funcionarioMes = funcionarioMes;
    }

    public LocalDate getAdmissao() {
        return admissao;
    }

    public void setAdmissao(LocalDate admissao) {
        this.admissao = admissao;
    }

    public double getRecebeBonus() {
        System.out.println(salario);
        System.out.println(recebeBonus);
        return recebeBonus;
    }

    public void setRecebeBonus(double recebeBonus) {
        this.recebeBonus = recebeBonus;
    }

    public double getPorcentagemBonusAssinuidade() {
        return porcentagemBonusAssinuidade;
    }

    public void setPorcentagemBonusAssinuidade(double porcentagemBonusAssinuidade) {
        this.porcentagemBonusAssinuidade = porcentagemBonusAssinuidade;
    }

    public double getPorcentagemBonusTempodeServico() {
        return porcentagemBonusTempodeServico;
    }

    public void setPorcentagemBonusTempodeServico(double porcentagemBonusTempodeServico) {
        this.porcentagemBonusTempodeServico = porcentagemBonusTempodeServico;
    }

    public double getPorcentagemBonusFuncionarioDoMes() {
        return porcentagemBonusFuncionarioDoMes;
    }

    public void setPorcentagemBonusFuncionarioDoMes(double porcentagemBonusFuncionarioDoMes) {
        this.porcentagemBonusFuncionarioDoMes = porcentagemBonusFuncionarioDoMes;
    }

    public double getPorcentagemBonusPorCargo() {
        return porcentagemBonusPorCargo;
    }

    public void setPorcentagemBonusPorCargo(double porcentagemBonusPorCargo) {
        this.porcentagemBonusPorCargo = porcentagemBonusPorCargo;
    }

    public LocalDate getDataDeCalculo() {
        return dataDeCalculo;
    }

    public void setDataDeCalculo(LocalDate dataDeCalculo) {
        this.dataDeCalculo = dataDeCalculo;
    }
    
    

}
