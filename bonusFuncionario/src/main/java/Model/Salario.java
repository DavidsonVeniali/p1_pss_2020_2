/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author Davidson
 */
public class Salario {
    private String nome;
    private LocalDate data;
    private double salarioBase;
    private List<String> bonus;
    private double salarioFinal;

    public Salario(String nome, LocalDate data, double salarioBase, List<String> bonus, double salario) {
        this.nome = nome;
        this.data = data;
        this.salarioBase = salarioBase;
        this.bonus = bonus;
        this.salarioFinal = salario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    public List<String> getBonus() {
        return bonus;
    }

    public void setBonus(List<String> bonus) {
        this.bonus = bonus;
    }

    public double getSalario() {
        return salarioFinal;
    }

    public void setSalario(double salario) {
        this.salarioFinal = salario;
    }

}
