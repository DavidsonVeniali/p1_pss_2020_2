/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Collection;

import Model.AplicacaoBonus;
import Model.Funcionario;
import Model.Observer.Subject;
import Model.Salario;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Davidson
 */
public class DadoCollection extends Subject {

    private static DadoCollection instances;

    private List<Funcionario> listFuncionario;
    private List<Salario> listSalario;
    private List<AplicacaoBonus> listBonus;

    private DadoCollection() {
        listFuncionario = new ArrayList<>();
        listBonus = new ArrayList<>();
    }

    public static DadoCollection getIntances() {
        if (instances == null) {
            instances = new DadoCollection();
        }

        return instances;
    }

    public List<AplicacaoBonus> getListBonus() {
        return listBonus;
    }

    public void setListBonus(List<AplicacaoBonus> listBonus) {
        this.listBonus = listBonus;
        notificarObserver();
    }

    public List<Funcionario> getListFuncionario() {
        return listFuncionario;
    }

    public void setListFuncionario(List<Funcionario> listFuncionario) {
        this.listFuncionario = listFuncionario;
        notificarObserver();
    }

    public List<Salario> getListSalario() {
        return listSalario;
    }

    public void setListSalario(List<Salario> listSalario) {
        this.listSalario = listSalario;
        notificarObserver();
    }

    public void add(Funcionario funcionario) {
        funcionario.setId(listFuncionario.size());
        addBonus(funcionario);
        listFuncionario.add(funcionario);
        notificarObserver();
    }

    public void addBonus(Funcionario funcionario) {
        AplicacaoBonus bonus = new AplicacaoBonus(funcionario.getId(), funcionario.getCargo(), funcionario.getNome(), funcionario.getBonus(), funcionario.getSalario(), funcionario.getFalta(), funcionario.getFuncionarioMes(), funcionario.getAdmissao());
        listBonus.add(bonus);

    }

    public void updateBonus(AplicacaoBonus bonus, int id, LocalDate dataParaCalculo) {
        AplicacaoBonus bonusAtualizado = new AplicacaoBonus(bonus.getId(), bonus.getCargo(), bonus.getNome(), bonus.getBonus(), bonus.getSalario(), bonus.getFalta(), bonus.getFuncionarioMes(), bonus.getAdmissao(), dataParaCalculo);
        listBonus.set(id, bonus);

    }

    public List<AplicacaoBonus> readBonus(LocalDate data) {

        List<AplicacaoBonus> listaRetornoBonus = new ArrayList<>();

        for (AplicacaoBonus bonus : listBonus) {
            if (bonus.getAdmissao().equals(data)) {
                listaRetornoBonus.add(bonus);
            }
        }
        return listaRetornoBonus;
    }

    public void delete(Funcionario funcionario) {
        listFuncionario.remove(funcionario.getId());

        for (int i = funcionario.getId(); i < listFuncionario.size(); i++) {
            listFuncionario.get(i).setId(i);
        }
        //
        for (int i = funcionario.getId(); i < listBonus.size(); i++) {
            listBonus.get(i).setId(i);
        }
        notificarObserver();
    }

    public Funcionario read(Funcionario funcionario) {
        return listFuncionario.get(funcionario.getId());
    }

    public Funcionario readId(int id) {
        return listFuncionario.get(id);
    }

    public List<Funcionario> read(String nomeFuncionario) {

        List<Funcionario> listaRetornoFuncionario = new ArrayList<>();

        for (Funcionario funcionario : listFuncionario) {
            if (funcionario.getNome().contains(nomeFuncionario)) {
                listaRetornoFuncionario.add(funcionario);
            }
        }
        return listaRetornoFuncionario;
    }

    public void update(Funcionario funcionario, int id) {
        listFuncionario.set(id, funcionario);
    }

}
